﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaPagamentos
{
    class PagamentoCollection
    {
        static List<Pagamento> pagamentos = new List<Pagamento>();

        public static void Insert(Pagamento pagamento)
        {
            pagamento.Id = pagamentos.Count() + 1;
            pagamentos.Add(pagamento);
        }

        public static Pagamento Search(Pagamento pagamento)
        {
            return pagamentos.FirstOrDefault(x => x.Funcionario.Cpf.Equals(pagamento.Funcionario.Cpf) && x.Mes == pagamento.Mes && x.Ano == pagamento.Ano);
        }
    }
}
