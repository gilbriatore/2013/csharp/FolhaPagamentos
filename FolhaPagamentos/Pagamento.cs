﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaPagamentos
{
    abstract class Pagamento
    {
        public int Id { set; get; }
        public Funcionario Funcionario { set; get; }
        public int Mes { set; get; }
        public int Ano { set; get; }
        public int Horas { set; get; }
        public float Valor { set; get; }
    }
}
