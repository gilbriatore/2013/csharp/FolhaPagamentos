﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaPagamentos
{
    class CargoCollection
    {
        static List<Cargo> cargos = new List<Cargo>();

        public static void Insert(Cargo cargo)
        {
            cargo.Id = cargos.Count() + 1;
            cargos.Add(cargo);
        }

        public static Cargo Search(Cargo cargo)
        {
            return cargos.FirstOrDefault(x => x.Nome.Equals(cargo.Nome));
        }
    }
}
