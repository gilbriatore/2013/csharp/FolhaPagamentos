﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaPagamentos
{
    class PagamentoNegocio
    {
        public float RealizarPagamento(Pagamento pagamento)
        {
            float valor = pagamento.Horas * pagamento.Valor;
            if (pagamento.GetType() == typeof(PagamentoUm))
                return valor;
            else if (pagamento.GetType() == typeof(PagamentoDois))
                return valor + (valor * ((PagamentoDois)pagamento).Comissao / 100);
            else
                return valor + (valor * ((PagamentoTres)pagamento).Comissao / 100) + ((PagamentoTres)pagamento).Bonus;
        }      

    }
}
