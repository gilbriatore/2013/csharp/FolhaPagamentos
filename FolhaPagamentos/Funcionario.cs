﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaPagamentos
{
    class Funcionario
    {
        public int Id { set; get; }
        public Cargo Cargo { set; get; }
        public string Nome { set; get; }
        public string Cpf { set; get; }
    }
}
