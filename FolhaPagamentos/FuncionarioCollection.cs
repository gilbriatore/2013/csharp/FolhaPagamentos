﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaPagamentos
{
    class FuncionarioCollection
    {
        static List<Funcionario> funcionarios = new List<Funcionario>();

        public static void Insert(Funcionario funcionario)
        {
            funcionario.Id = funcionarios.Count() + 1;
            funcionarios.Add(funcionario);
        }

        public static Funcionario Search(Funcionario funcionario)
        {
            return funcionarios.FirstOrDefault(x => x.Cpf.Equals(funcionario.Cpf));
        }
    }
}
